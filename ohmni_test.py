"""A demo for object detection.

For Raspberry Pi, you need to install 'feh' as image viewer:
sudo apt-get install feh

Example (Running under python-tflite-source/edgetpu directory):

  - Face detection:
    python3.5 demo/object_detection.py \
    --model='test_data/mobilenet_ssd_v2_face_quant_postprocess_edgetpu.tflite' \
    --input='test_data/face.jpg'

  - Pet detection:
    python3.5 demo/object_detection.py \
    --model='test_data/ssd_mobilenet_v1_fine_tuned_edgetpu.tflite' \
    --label='test_data/pet_labels.txt' \
    --input='test_data/pets.jpg'

'--output' is an optional flag to specify file name of output image.
"""
import argparse
import platform
import subprocess
import signal
from edgetpu.detection.engine import DetectionEngine
from PIL import Image
from PIL import ImageDraw

import socket
import os, os.path
import time
from enum import Enum
from struct import *

# Open connection to bot shell
botshell = socket.socket( socket.AF_UNIX, socket.SOCK_STREAM )
botshell.connect("/app/bot_shell.sock")
#botshell.sendall(b"say hello\n")
botshell.sendall(b"wake_head\n")

if os.path.exists( "/dev/libcamera_stream" ):
  os.remove( "/dev/libcamera_stream" )\

print("Opening socket...")
server = socket.socket( socket.AF_UNIX, socket.SOCK_DGRAM )
server.bind("/dev/libcamera_stream")
os.chown("/dev/libcamera_stream", 1047, 1047);

class SockState(Enum):
  SEARCHING = 1
  FILLING = 2

# Function to read labels from text files.
def ReadLabelFile(file_path):
  with open(file_path, 'r') as f:
    lines = f.readlines()
  ret = {}
  for line in lines:
    pair = line.strip().split(maxsplit=1)
    ret[int(pair[0])] = pair[1].strip()
  return ret

def keyboardInterruptHandler(signal, frame):
  print("Stopping...")
  botshell.sendall("manual_move 0 0\n".encode())
  exit(0)

signal.signal(signal.SIGINT, keyboardInterruptHandler)

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument(
      '--model', help='Path of the detection model.', required=True)
  parser.add_argument(
      '--label', help='Path of the labels file.')
  args = parser.parse_args()

  # Initialize engine.
  engine = DetectionEngine(args.model)
  labels = ReadLabelFile(args.label) if args.label else None

  state = SockState.SEARCHING
  imgdata = None
  framewidth = 0
  frameheight = 0
  frameformat = 0
  framesize = 0

  facecount = 0
  fdelay = 3
  fndiff = 0

  peek_phrases = ["peekaboo", "i see you", "there you are", "hi there", "you're looking good today", "working hard or hardly working?"]
  peek_idx = 0

  lastrot = 0
  lastvel = 0
  nodetlag = 5
  nodetcount = 0

  lastservoy = 512
  servoy = 512
  
  print("Listening...")
  server.settimeout(0.5)
  while True:

    try:
      datagram = server.recv( 65536 )
    except socket.timeout:
      if lastrot != 0 or lastvel != 0:
        print("socket timeout, clear manual_move")
        botshell.sendall("manual_move 0 0\n".encode())
        lastrot = 0  
        lastvel = 0
      continue

    if not datagram:
      break

    # Dump contents for view here
    #print("-" * 20)
    #print(datagram)
    #print(len(datagram))

    # Handle based on state machine
    if state == SockState.SEARCHING:

      # Check for non-control packets
      if len(datagram) < 12 or len(datagram) > 64:
        continue

      # Check for magic
      if not datagram.startswith(b'OHMNICAM'):
        continue

      # Unpack the bytes here now for the message type
      msgtype = unpack("I", datagram[8:12]) 
      if msgtype[0] == 1:
        params = unpack("IIII", datagram[12:28])
        #print("Got frame start msg:", params)

        state = SockState.FILLING
        imgdata = bytearray()

        framewidth = params[0]
        frameheight = params[1]
        frameformat = params[2]
        framesize = params[3]

      #elif msgtype[0] == 2:
        # END FRAME - for now no-op
        #print("Got end frame.")
      
      #else:
        # No op for other
        #print("Got other msgtype.")

    # Filling image buffer now
    elif state == SockState.FILLING:

      # Append to buffer here
      imgdata.extend(datagram)

      # Check size
      if len(imgdata) < framesize:
        continue

      # Resize and submit
      imgbytes = bytes(imgdata)
      newim = Image.frombytes("L", (framewidth, frameheight), imgbytes, "raw", "L")
      rgbim = newim.convert("RGB")

      # Run inference.
      ans = engine.DetectWithImage(rgbim, threshold=0.05, keep_aspect_ratio=True,
                                   relative_coord=False, top_k=10)

      # Best face to follow
      best = None

      # Display result.
      currfaces = 0
      if ans:
        currfaces = len(ans)
        for obj in ans:
          print ('-----------------------------------------')
          #if labels:
          #  print(labels[obj.label_id])
          print ('score = ', obj.score)
          box = obj.bounding_box.flatten().tolist()
          #print ('box = ', obj.bounding_box)   

          if obj.score > 0.45:
            bb = obj.bounding_box
            cx = (bb[0][0] + bb[1][0]) * 0.5
            cy = (bb[0][1] + bb[1][1]) * 0.5
            wid = bb[1][0] - bb[0][0]
            hei = bb[1][1] - bb[0][1]
            avgsz = (wid + hei) * 0.5
            if avgsz > 72 and (best is None or avgsz > best[0]):
              best = [avgsz, cx, cy, obj.score]
      else:
        print("No detections.")

      # Update speed here if needed
      newrot = 0
      newvel = 0
      sdelta = 0
      if best:
        nx = (best[1] - 640) / 640.0
        ny = (best[2] - 512) / 512.0
        print("Face at {} {} size {} score {}".format(nx, ny, best[0], best[3]))
        newrot = nx * 800
        if newrot < -400: newrot = -400
        if newrot > 400: newrot = 400
        
        if best[0] < 180:
          newvel = (180-best[0]) * 1000
          if newvel > 800: newvel = 800

        if best[0] > 300:
          newvel = -((best[0] - 300) * 200)
          if newvel < -400: newvel = -400

        ny += 0.2
        sdelta = ny * -10.0
        if sdelta < -25: sdelta = -25
        if sdelta > 25: sdelta = 25 
        #print("sdelta {}".format(sdelta))

      # Hack in case we lose a few frames
      if best is None:
        if nodetcount < nodetlag:
          newrot = lastrot
          newvel = lastvel
          nodetcount += 1
      else:
        nodetcount = 0

      # check delta
      newrot = round(newrot)
      newvel = round(newvel)
      if newrot != lastrot or newvel != lastvel:
        botshell.sendall("manual_move {} {}\n".format(newrot + newvel, newrot - newvel).encode())
        lastrot = newrot
        lastvel = newvel

      # check sdelta
      if sdelta != 0:
        servoy += sdelta
        if servoy > 700: servoy = 700
        if servoy < 300: servoy = 300
        if servoy != lastservoy:
          botshell.sendall("pos 3 {} 15\n".format(servoy).encode())
          lastservoy = servoy

      # Handle stateful messaging
      #if currfaces != facecount:
      #  fndiff += 1
      #  if fndiff > fdelay:
      #    facecount = currfaces
      #    fndiff = 0
      #    if facecount > 0:
      #      #botshell.sendall("say wow {} faces\n".format(facecount).encode())
      #      botshell.sendall("say {}\n".format(peek_phrases[peek_idx]).encode())
      #      peek_idx += 1
      #      if peek_idx >= len(peek_phrases):
      #        peek_idx = 0

      # Go back to initial state
      state = SockState.SEARCHING
      #print("Got complete frame")

  print("-" * 20)
  print("Shutting down...")
  server.close()

  os.remove( "/dev/libcamera_stream" )
  print("Done")

if __name__ == '__main__':
  main()
